// Import the LinkedList class
import java.util.LinkedList;

public class HelloWorld {
  public static void main(String[] args) {
    LinkedList<String> cars = new LinkedList<String>();
    cars.add("Volvo");
    cars.add("BMW");
    cars.addLast("Ford");
    cars.addFirst("Mazda");
    cars.add(2,"Toyota");
    System.out.println(cars);
    cars.removeLast();
    System.out.println(cars);
  }
}